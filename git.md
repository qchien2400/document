# GIT SETTING
```properties
[alias]
    lg = log --color --graph --pretty=format:'%C(red)%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
```
# Git credential for ubuntu

```git
<!-- Store credential -->
git config credential.helper store
```

# Completely Remove Unwanted History

To clear history and re-initial repositoty

```git
<!-- Switch to another branch without any commit history -->
git checkout --orphan freshBranch
<!-- Add all files -->
git add -A
<!-- Commit -->
git commit
<!-- Delete branch -->
git branch -D master 
<!-- Rename currnet branch -->
git branch -m master
<!-- Push to remote -->
git push -f origin master 
<!-- Remove all history and old file -->
git gc --aggressive --prune=all
<!-- Push new state -->
git push -f origin master
```


function current_branch() {
    git branch --show-current
}

# Git Aliases
alias gl='git pull'
alias glr='git pull --rebase'
alias gp='git push'
alias gpf='git push --force'
alias gaa='git add --a'
alias gcmsg='git commit -m'
alias gpsup='git push --set-upstream origin $(current_branch)'
alias gb='git branch'
alias gbc='git branch --show-current'
alias gba='git branch -a'
alias gbD='git branch -D'
alias gco='git checkout'
alias gcb='git checkout -b'
alias gst='git status'
alias grb='git rebase'
alias grba='git rebase --abort'
alias grbc='git rebase --continue'
alias grbs='git rebase --skip'
alias grh='git reset --hard'
alias grhh='git reset --hard HEAD'
alias glod="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset'"

# Docker Aliases
alias dc='docker-compose'
alias dm='docker-machine'