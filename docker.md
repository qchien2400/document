# Docker Context
---
__Document__
* [Context](https://www.docker.com/blog/how-to-deploy-on-remote-docker-hosts-with-docker-compose/)

__Code__
```
<!-- Create -->
docker context create remote ‐‐docker “host=ssh://user@remotemachine”
<!-- Update -->
docker context update remote ‐‐docker “host=ssh://user@remotemachine”
```
# Docker machine
---
## Step to create one machine `generic`

__Document__

* [docs.docker.com](https://docs.docker.com/machine/drivers/generic/)

__Prerequisite__

* Remote enable to ssh, check your remote
  1. Openssh-server. [Document](https://linuxize.com/post/how-to-enable-ssh-on-ubuntu-18-04/)
  2. Enable `ssh` with `root` user. [Document](https://linuxconfig.org/allow-ssh-root-login-on-ubuntu-18-04-bionic-beaver-linux)
  3. Verify open port 22 via `netstat`: `netstat -tunlp | grep '22'`
* Remote machine be able to accessed via `ssh` to `root` without password prompt. Must use `root` user to avoid password promt when execute `docker` command. The guide is below
  1. `ssh-keygen -t rsa -b 4096` should use empty key pharse
  2. `ssh-copy-id *user@*host`

__Create__

1. Crate `docker-machine create -d generic --generic-ip-address=*IP *NAME`
2. Set default `eval $(docker-machine env *NAME)` or run specific `docker-machine ssh *NAME *Commands`

__Share__

* [Document](https://hackernoon.com/finally-you-can-share-docker-machines-without-a-script-8f946d050f7)
  * Copy machine folder to other

## Step to create one machine `virtualbox`

__Prerequisite__

* Install `virtualbox`

# Docker Cluster
* TCP port 2377 for cluster management communications.
* TCP and UDP port 7946 for communication among nodes.
* UDP port 4789 for overlay network traffic.