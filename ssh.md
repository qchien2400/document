# Enable ssh root user
1. **Change password root** [Document](https://www.cyberciti.biz/faq/change-root-password-ubuntu-linux/)
2. **Enable ssh to root:** [Document](https://linuxconfig.org/allow-ssh-root-login-on-ubuntu-18-04-bionic-beaver-linux)
# Using ssh
## Ssh to remote without passpharse
1. `ssh-keygen -t rsa -b 4096`
2. `ssh-copy-id *user@*host`
## Ssh to remote with passpharse
1. **Same `without passpharse`
2. Start `ssh-agent`
3. Using `ssh-add`

# Port Forwarding
* Local `ssh -L [LOCAL_IP:]LOCAL_PORT:DESTINATION:DESTINATION_PORT [USER@]SSH_SERVER`
* Remote `ssh -R [REMOTE:]REMOTE_PORT:DESTINATION:DESTINATION_PORT [USER@]SSH_SERVER`
* Dynamic `ssh -D [LOCAL_IP:]LOCAL_PORT [USER@]SSH_SERVER`
* Optional
  * `-f` to run in background
  * `-N` to avoid execute `ssh` command
* List process run ssh `ps aux | grep ssh`
* Kill process `kill *PID`

# Verify Connection
* TCP: `telnet *HOST *PORT`
* UDP: `nc -v -u -z *HOST *PORT`