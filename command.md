# Select Option

Example
```bash
#!/bin/bash
pods=($(kubectl get pods --template "{{range .items}}{{.metadata.name}}{{\"\n\"}}{{end}}" -n digital-banking))
select pod in "${pods[@]}"
do
  logs=($(kubectl exec -it $pod -n digital-banking -- find /tmp/logs -mtime -0))
  select log in "${logs[@]}"
  do
    kubectl cp digital-banking/$pod:$log /home/logs/
    echo "Your file locate at /home/logs/"
    break;
  done
  break;
done
```