# Commands
* **Show databases** `show dbs`
* **Show collections** `db.getCollectionNames()`
* **Get list index** `db.CollectionName.getIndexes()`
* `$[projection]` for projection
* `$` for query arrays
# Caped Collection
Collection for logging data, have limited `size` and `amount` for automatically remove when over zie or amount of documents
# TTL Collection
Index with specific time to life (TTL) which auto remove when expired