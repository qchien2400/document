# Install Flame
1. Run command `sudo apt install flameshot`
2. Set up short cut key
  * Head to the system settings and navigate your way to the Keyboard settings.
  * You will find all the keyboard shortcuts listed there, ignore them and scroll down to the bottom. Now, you will find a + button.
  * Click the “+” button to add a custom shortcut. You need to enter the following in the fields you get:
    * Name: `Anything You Want`
    * Command: `/usr/bin/flameshot gui`
  * Finally, set the shortcut to PrtSc – which will warn you that the default screenshot functionality will be disabled – so proceed doing it.

* __Auto login user__ [Document](https://help.ubuntu.com/stable/ubuntu-help/user-autologin.html.en)
* __User blank password__ [Document](https://askubuntu.com/questions/281074/can-i-set-my-user-account-to-have-no-password)

# JDK
* Download `jdk` with format `tar.gz`
* Add folder `jdk` to directory `/usr/local`
* Add environment at `/etc/environment` as `JAVA_HOME =/usr/local/*JDK`
* Add export at `~/.bashrc` as `export PATH=$PATH:$JAVA_HOME/bin`

# Maven

* Download `maven` with format `tar.gz` and `binary` file
* Add folder `maven` to directory `/usr/local`
* Add environment at `/etc/environment` as `MAVEN_HOME =/usr/local/*MAVEN`
* Add environment at `/etc/environment` as `M2_HOME =/usr/local/*MAVEN/bin`
* Add export at `~/.bashrc` as `export PATH=$PATH:$M2_HOME`

# Plugin for Intellij

* Lombok
* SonarLint
* Checkstyle
* MavenHelper