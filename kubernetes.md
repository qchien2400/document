# Note
## Replcation Controller - rc
* `Replcation Controller` - `rc` will manage `pod` by label selector, due to, when you re-labeled or remove some pod's labeling, then `pod` not be managed by `rc` anymore.
* When you update pod template, `rc` don't do anything, but you delete an pod that be managed by `rc`, then it will create a new `pod` with `new template`
## Replcation Set - rs
* Similar `rc`
## Daemon Set
* Note: To run one `pod` in each `node`
# Command
* Get pods: `kubectl get pods`
* Get log: `kubectl logs *pod`
* Port forward: `kubectl port-forward *pod *source_port:*destination_port`
* Retrieve description pod: `kubectl describe pod *pod`