# Visual Studio Code
## Shortcut
**File:** `keybindings.json`
```json
// Place your key bindings in this file to override the defaults
[
  {
    "key": "ctrl+shift+s",
    "command": "saveAll"
  },
  {
    "key": "ctrl+alt+shift+n",
    "command": "explorer.newFolder",
    "when": "explorerViewletFocus"
  },
  {
    "key": "ctrl+alt+n",
    "command": "explorer.newFile",
    "when": "explorerViewletFocus"
  }
]
```
**File:** `settings.json`
```json
// Place your key bindings in this file to override the defaults
{
    "terminal.integrated.defaultProfile.windows": "Git Bash",
    // optional
    "jsonnet.executablePath": "C:\\Users\\ctruong\\jsonnet.exe",
    "editor.renderWhitespace": "all"
}
```
## Extension
* Gitlens
* Bookmarks
* Swagger Viewer
* VS Sequential Number
# Intellij
## Extension
* Checkstyle
* Lombok
* Sonar
* Maven helper *optional*
## Shortcut
* Git popup `Alt + G`
# Dbeaver