from pymongo import MongoClient


def connect_database(url_connection):
    """
    Use for connect to mongo db
    :param url_connection: url connection for mongo
    :return: mongo client that connected to mongo db
    """
    return MongoClient(url_connection)
