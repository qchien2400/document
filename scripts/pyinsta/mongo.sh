#!/bin/bash
docker volume create insta
docker run -d --restart always -p 27017:27017 \
-e MONGO_INITDB_DATABASE=Insta \
-v insta:/data/db \
mongo:latest

# Initial db script
# -v C:\\Users\\ctruong\\Workspace\\document\\scripts\\pyinsta\\db:/docker-entrypoint-initdb.d \