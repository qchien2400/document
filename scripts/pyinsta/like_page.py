import logging

from bson.objectid import ObjectId
from pymongo import MongoClient
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options

import utils

logging.basicConfig(format='%(asctime)s - %(process)d - %(name)s:%(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def add_account(mongo_client, account):
    mongo_client[DATABASE][ACCOUNT_COLLECTION].insert_one({
        "pages": [],
        "blocked": False,
        "password": f"{account['password']}",
        "username": f"{account['username']}"
    })


def add_page_to_account(mongo_client, account, page_name):
    account_id = account['_id']
    if account_id is None:
        return
    mongo_client[DATABASE][ACCOUNT_COLLECTION].update_one(
        {'_id': ObjectId(account_id)},
        {'$push': {'pages': {
            "enable": True,
            "lastPost": "",
            "page": f"{page_name}"
        }}}
    )


logger.info('Starting')

# Amount post to interact
countPage = 30

# Can ignore value of host and port, default is localhost:27017
mongoUri = 'mongodb://%s:%s' % ('localhost', '27017')

DATABASE = 'Insta'
ACCOUNT_COLLECTION = 'Account'

# Input
# pageName = 'twoaugusts.store'
pageName = 'lafiene.order'

INSTA_BASE_URL = 'https://www.instagram.com'
LOGIN_URL = f'{INSTA_BASE_URL}/accounts/login'
PAGE_URL = f'{INSTA_BASE_URL}/{pageName}'

options = Options()
options.headless = False

try:
    with webdriver.Firefox(options=options) as session, MongoClient() as mongoClient:
        database = mongoClient[DATABASE]
        accountCollection = database[ACCOUNT_COLLECTION]
        baseAccount = accountCollection.find_one({'blocked': False})
        logger.debug(f"Login to get latest url post with account {baseAccount['username']}")
        utils.login(session, baseAccount)
        urlLatestPost = utils.get_url_latest_post(session, PAGE_URL)
        utils.logout(session)
        logger.debug(f"Logout account {baseAccount['username']}")
        if urlLatestPost is None:
            raise Exception('Done with none interactive')
        latestIdPost = utils.get_post_id_from_url(urlLatestPost)
        logger.debug(f"Latest post for page {pageName} is {urlLatestPost}")
        accounts = accountCollection.find({
            'blocked': False,
            'pages.enable': True,
            'pages.page': pageName,
            'pages.lastPost': {'$ne': latestIdPost}
        })
        for account in accounts:
            logger.info(f"*** Start with account {account['username']} ***")
            session.get(LOGIN_URL)
            try:
                utils.login(session, account)
                if utils.check_suspicious(session):
                    accountCollection.update_one(
                        {'_id': ObjectId(account['_id'])},
                        {'$set': {'blocked': True}}
                    )
                    raise Exception(f"Suspicious account with username {account['username']}")
                session.get(PAGE_URL)
                utils.wait_element(session, By.CLASS_NAME, "v1Nh3").click()
                utils.like_post(session)
                current_post_id = utils.get_post_id_from_url(session.current_url)
                before_update_result = accountCollection.find_one_and_update(
                    {'_id': ObjectId(account['_id']), 'pages.page': pageName},
                    {'$set': {'pages.$.lastPost': current_post_id}},
                    projection={'pages.$': 1}
                )
                previous_last_interacted_post_id = before_update_result['pages'][0]['lastPost']
                for _ in range(countPage):
                    utils.next_post(session)
                    logger.debug(f"Like for post {session.current_url}")
                    if previous_last_interacted_post_id == utils.get_post_id_from_url(session.current_url):
                        break
                    utils.like_post(session)
            except:
                logger.exception(f"Error when starting with account {account['username']}")
            finally:
                utils.logout(session)
                logger.info(f"*** Completed for account {account['username']} ***")
except:
    logger.exception('Exception')
finally:
    logger.info('Completed')
