`export PATH=$PATH:/c/Users/ctruong/Software/geckodriver`

# Require
* Run on Firefox

# Use-cases

# Scripts
* Add `geckodriver` to `PATH` environment (require by firefox)
    ```bash
    <!-- unix -->
    export PATH=$PATH:/path/to/folder/geckodriver
    ```
    ```cmd
    <!-- windows -->
    set PATH=%PATH%;C:\Users\ctruong\Software\geckodriver
    ```

CLASS_NAME = 'class name'

CSS_SELECTOR = 'css selector'

ID = 'id'

LINK_TEXT = 'link text'

NAME = 'name'

PARTIAL_LINK_TEXT = 'partial link text'

TAG_NAME = 'tag name'

XPATH = 'xpath'

mochi.orders
tinker_nguyen
tri220995
qchien_2400
sin.tth
twoaugusts.store