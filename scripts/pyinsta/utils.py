import logging
from time import sleep

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

logging.basicConfig(format='%(asctime)s-%(process)d-%(name)s:%(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def wait_element(session, by, selector):
    return WebDriverWait(session, 60).until(
        EC.presence_of_element_located((by, selector)),
        f'Error when try get element at current url {session.current_url} with selector [{selector}] by {by}'
    )


def wait_elements(session, by, selector):
    return WebDriverWait(session, 60).until(
        EC.presence_of_all_elements_located((by, selector)),
        f'Error when try get elements at current url {session.current_url} with selector [{selector}] by {by}'
    )


def next_post(session):
    wait_element(session, By.CLASS_NAME, "coreSpriteRightPaginationArrow").click()


def comment_post(session):
    """

    :param session:
    :return:
    """
    wait_element(session, By.XPATH, "//textarea").click()
    wait_element(session, By.XPATH, "//textarea").send_keys(u'ghé shop xem đồ xinh nhé')
    wait_element(session, By.XPATH, "//button[@type='submit']").click()
    sleep(5)


def like_post(session):
    """

    :param session:
    :return:
    """
    like_button = wait_element(session, By.XPATH, "//section/span[1]")
    svg_like_button = like_button.find_element(By.CLASS_NAME, '_8-yf5')
    if svg_like_button.get_attribute('aria-label') == 'Like':
        like_button.click()
    sleep(5)


def get_post_id_from_url(url):
    """

    :param url:
    :return:
    """
    if not url:
        return ''
    strings = list(filter(None, url.split('/')))
    # return last index
    return strings.pop()


def get_url_latest_post(session, page_url):
    """

    :param session:
    :param page_url:
    :return:
    """
    session.get(page_url)
    first_a_tag = wait_element(session, By.XPATH, "//div[@class='v1Nh3 kIKUG  _bz0w']/a")
    return first_a_tag.get_attribute('href')


def get_url_current_post(session):
    """

    :param session:
    :return:
    """
    return session.current_url


def logout(session):
    """

    :param session:
    :return:
    """
    session.delete_all_cookies()


def is_private_account(session):
    private_clause = 'This Account is Private'
    try:
        text = wait_element(session, By.XPATH, "//h2[@class='rkEop']").text
        logger.debug(f"Compare clause text [{text}] with clause fixed is [{private_clause}]")
        if private_clause == text:
            return True
    except Exception as exp:
        logger.exception(exp)
        return False


def follow(session):
    buttons = session.find_elements(By.XPATH, "//button[@type='button']")
    for button in buttons:
        if 'Follow' == button.text:
            button.click()
            return


def get_detail_page(session):
    """
    Get object detail
    :param session:
    :return: {'posts': *number, 'followers': *number, 'following': *number}
    """
    result = {
        'posts': 0,
        'followers': 0,
        'following': 0,
    }
    # get posts, followers, following elements
    elements = session.find_elements(By.CLASS_NAME, "-nal3")
    for element in elements:
        arr = element.text.split(" ")
        result[arr[1]] = int(arr[0])
    return result


def check_suspicious(session):
    return session.page_source.__contains__('Suspicious')


def login(session, account):
    """

    :param session:
    :param account:
    :return:
    """
    session.get('https://www.instagram.com/accounts/login/')
    wait_element(session, By.NAME, "username").send_keys(account['username'])
    wait_element(session, By.NAME, "password").send_keys(account['password'])
    wait_element(session, By.XPATH, "//button[@type='submit']").click()
    # wait for loading
    sleep(10)

