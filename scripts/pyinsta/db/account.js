// Add dummy account for initial Account Collection
db.Account.insert(
    {
        "pages": [
            {
                "sugCmts": [],
                "enable": true,
                "lastPost": "",
                "page": ""
            }
        ],
        "following": [],
        "followers": [],
        "blocked": false,
        "password": "pwd",
        "username": "dummyAccount"
    }
);
// Create unique index for field: username
db.Account.createIndex({"username": 1}, { unique: true });
// Remove dummy account
db.Account.remove({"username": "dummyAccount"});
// Insert initial data
db.Account.insert(
    {
        "pages": [
            {
                "sugCmts": [],
                "enable": true,
                "lastPost": "",
                "page": ""
            }
        ],
        "following": [],
        "followers": [],
        "blocked": false,
        "password": "kiwevay1",
        "username": "ctruong2004"
    }
)