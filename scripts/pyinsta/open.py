

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


def wait_element(session, by, selector):
    """

    :param session:
    :param by:
    :param selector:
    :return:
    """
    try:
        return WebDriverWait(session, 60).until(
            EC.presence_of_element_located((by, selector)),
            f'Error when try get element at current url {session.current_url} with selector [{selector}] by {by}'
        )
    except Exception as e:
        print(e)
        raise e



session = webdriver.Firefox()
baseAccount = {"username": "ctruong2004", "password": "kiwevay1"}
session.get('https://www.instagram.com/accounts/login/')
wait_element(session, By.NAME, "username").send_keys(baseAccount['username'])
wait_element(session, By.NAME, "password").send_keys(baseAccount['password'])
wait_element(session, By.XPATH, "//button[@type='submit']").click()
