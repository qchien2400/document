import logging
from time import sleep
import utils
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

logging.basicConfig(format='%(asctime)s-%(process)d-%(name)s:%(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


# Input
tag = 'vietnamesegirl'

numberOfUser = 10
numberOfLike = 10
numberOfComment = 1

session = webdriver.Firefox()
baseAccount = {"username": "ctruong2004", "password": "kiwevay1"}
session.get('https://www.instagram.com/accounts/login/')
utils.wait_element(session, By.NAME, "username").send_keys(baseAccount['username'])
utils.wait_element(session, By.NAME, "password").send_keys(baseAccount['password'])
utils.wait_element(session, By.XPATH, "//button[@type='submit']").click()
sleep(10)
session.get(f'https://www.instagram.com/explore/tags/{tag}')

utils.wait_element(session, By.CLASS_NAME, "v1Nh3").click()


while numberOfUser != 0:
    # Get person commented
    elements = WebDriverWait(session, 60).until(
        EC.presence_of_all_elements_located((By.XPATH, "//a[@class='sqdOP yWX7d     _8A5w5   ZIAjV ']"))
    )
    # Can't use set(), cuz weird issue
    listHref = list()
    for ele in elements:
        if listHref.__contains__(ele):
            continue
        listHref.append(ele.get_attribute('href'))
    parentTab = session.current_window_handle
    session.execute_script("window.open('')")
    session.switch_to.window(session.window_handles.pop())
    owner = listHref[0]
    for href in listHref:
        if href != owner:
            session.get(href)
            if utils.is_private_account(session):
                continue
            # {'posts': *number, 'followers': *number, 'following': *number}
            detail = utils.get_detail_page(session)
            if detail['posts'] < 10 or detail['followers'] < 20:
                continue
            utils.follow(session)
            utils.wait_element(session, By.CLASS_NAME, "v1Nh3").click()
            utils.like_post(session)
            utils.comment_post(session)
            for _ in range(numberOfLike):
                utils.next_post(session)
                logger.debug(f"Like for post {session.current_url}")
                utils.like_post(session)
            numberOfUser -= 1
            sleep(10)
    session.close()
    session.switch_to.window(parentTab)
    utils.next_post(session)
session.quit()
