# Just me and Opensource
Description: Chanel about Kubernetes, MongoDB, Elasticsearch, ArgoCD, AWS, GCP
Link Youtube: https://www.youtube.com/c/wenkatn-justmeandopensource/featured

# Maven
Update multiple version `mvn versions:set -DnewVersion=*version`

# VSC
Open result of command from bash\terminal. For instance, when use for `jsonnet` `jsonnet *file.jsonnet | code - -a`
  * `-a` option is open current editor, avoid open new window

# VIM
Open result of command from bash\terminal. For instance, when use for `jsonnet` `jsonnet *file.jsonnet | vim -`